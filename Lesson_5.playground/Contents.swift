//#1
class Auto {
    var wheel: Int = 4
    var doors: Int = 5
    var fuel: String
    
    init(fuel: String, wheel: Int, doors: Int) {
        self.wheel = wheel
        self.fuel = fuel
        self.doors = doors
    }
}
class Bus: Auto {
}
class Belaz: Auto {
}

//#2
class House {
    var width: Int
    var height: Int
    
    init(width:Int, height:Int) {
        self.width = width
        self.height = height
    }
    
    func create() {
        var square = width * height
        print(square)
    }
    func destroy() {
        print("House destroyed")
    }
}
let house2 = House(width: 3, height: 4)
house2.create()

//#3
var names = ["Kostya", "Vlad", "Serov"]
func symbolCount(_ s1: String, _ s2: String) -> Bool {
   return s1 > s2
}
var reversedNames = names.sorted(by: symbolCount)
   print(reversedNames)

//#4
struct SquareOfHouse {
    var width = 0
    var height = 0
}
class Exterior {
    var square = SquareOfHouse()
    var bushes = false
    var entrys = 0
    var name: String?
}
// Основное отлиие структуры от классов в том, что структуры передаются по значению, а классы передаются по ссылке



